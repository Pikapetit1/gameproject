
#include "application.h"

int main(int argc, char* argv[]) {

	Application game;
	if (!game.initialize())
		return 1;
	game.run();
	game.uninitialize();

	return 0;
}

	//SDL_Window* window = NULL;
	//SDL_Renderer* renderer = NULL;
	//SDL_Texture* texture = NULL;

	//// Initialize SDL
	//if (SDL_Init(SDL_INIT_VIDEO) < 0)
	//{
	//	cout << "SDL could not initialize! SDL_Error: " << SDL_GetError() << endl;
	//	return 1;
	//}

	//// create the window and renderer
	//// note that the renderer is accelerated
	//window = SDL_CreateWindow("Window Title", 0, 78, WIDTH, HEIGHT, 0);
	//SDL_assert(window != NULL);
	//renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	//SDL_assert(renderer != NULL);

	//// load our image
	//texture = IMG_LoadTexture(renderer, "DisplayImage\\assets\\bg_720p.jpg");
	//if (texture == nullptr) {
	//	std::cout << "IMG_LoadTexture Error: " << SDL_GetError() << "\n";
	//	return 1;
	//}


	//int texture_width, texture_height; // texture width & height
	//SDL_QueryTexture(texture, NULL, NULL, &texture_width, &texture_height); // get the width and height of the texture
	//// put the location where we want the texture to be drawn into a rectangle
	//SDL_Rect texturePos; texturePos.x = 0; texturePos.y = 0; texturePos.w = texture_width; texturePos.h = texture_height;

	//unsigned int lastUpdateTime = 0;

	//// main loop
	//bool shouldQuit = false;
	//while (!shouldQuit)
	//{
	//	// event handling
	//	SDL_Event event;
	//	while (SDL_PollEvent(&event))
	//	{
	//		if (event.type == SDL_QUIT)
	//			shouldQuit = true;
	//		else if (event.type == SDL_KEYUP && event.key.keysym.sym == SDLK_ESCAPE)
	//			shouldQuit = true;
	//	}

	//	// draw the image once every 30ms, i.e. 33 images per second
	//	if (lastUpdateTime + 30 < SDL_GetTicks()) {
	//		lastUpdateTime = SDL_GetTicks();

	//		// clear the screen
	//		SDL_RenderClear(renderer);
	//		// copy the texture to the rendering context
	//		SDL_RenderCopy(renderer, texture, NULL, &texturePos);
	//		// flip the backbuffer - https://wiki.osdev.org/Double_Buffering
	//		// this means that everything that we prepared behind the screens is actually shown
	//		SDL_RenderPresent(renderer);
	//	}
	//}

	//SDL_DestroyTexture(texture);
	//SDL_DestroyRenderer(renderer);
	//SDL_DestroyWindow(window);

	//return 0;
//}
